

create database CS300_Project
use CS300_Project


create table ManHinh
(
	ID nvarchar(50),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	KichThuoc int,
	DoPhanGiai nvarchar(20),
	TiLeKhungHinh nvarchar(20),
	MauSac float,
	Gia int,
	BaoHanh int,
	GhiChu nvarchar(100)


);

create table CPU
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	Socket nvarchar(20),
	Gia int,
	BaoHanh int,
	GhiChu nvarchar(100)

);

create table RAM
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	Gia int,
	BaoHanh int,
	Loai nvarchar(20),
	Bus int,
	DungLuong nvarchar(20),
	GhiChu nvarchar(100)
);

create table OCung
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	Gia int,
	BaoHanh int,
	Loai nvarchar(20),
	DungLuong int,
	GhiChu nvarchar(100)
);

create table VGA
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(50),
	GPU nvarchar(50),
	BoNho nvarchar(50),
	MinimumPower int,
	Gia int,
	BaoHanh int,
	GhiChu nvarchar(200)


);

Create table MainBoard
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	Socket nvarchar(50),
	MemoryStandard nvarchar(50),
	RAMBus nvarchar(100),
	RAMSlot int,
	Gia int,
	BaoHanh int,
	GhiChu nvarchar(200)
);

create table PSU
(
	ID nvarchar(20),
	Ten nvarchar(100),
	ThuongHieu nvarchar(20),
	CongSuat int,
	Gia int,
	BaoHanh int,
	GhiChu nvarchar(150)
);




INSERT INTO ManHinh(ID,Ten,ThuongHieu,KichThuoc,DoPhanGiai,TiLeKhungHinh,MauSac,Gia,BaoHanh,GhiChu) values

('MH001','BENQ ZOWIE GAMING 24 144HZ 1MS XL2411','BenQ',24,'1920 x 1080','16:9',16.7,6879000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH002','BENQ ZOWIE GAMING 27 144HZ 1MS XL2720','BenQ',27,'1920 x 1080','16:9',16.7,10989000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH003','BENQ ZOWIE GAMING 27 2K 144HZ 1MS XL2730','BenQ',27,'2560 x 1440','16:9',16.7,14979000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH004','ASUS GAMING PRO MG278Q','Asus',27,'2560 x 1440','16:9',16.7,13079000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH005','ASUS GAMING PRO MG279Q','Asus',27,'2560 x 1440','16:9',16.7,16289000,36,'Refresh rate: 144Hz,
Response time: 4ms,Panel: IPS'),
('MH006','ASUS GAMING PRO PG278Q','Asus',27,'2560 x 1440','16:9',16.7,18989000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH007','ASUS GAMING PRO PG279Q','Asus',27,'2560 x 1440','16:9',16.7,18989000,36,'Refresh rate: 165Hz(Gsync),
Response time: 4ms,Panel: IPS'),
('MH008','ASUS GAMING PRO PG348Q','Asus',34,'3440 x 1440','21:9',1070,35489000,36,'Refresh rate: 100Hz,
Response time: 5ms,Panel: IPS'),
('MH009','ASUS GAMING PRO VG248QE-J 144HZ 1MS LED','Asus',24,'1920 x 1080','16:9',16.7,7989000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH010','ASUS GAMING PRO VG278HV','Asus',27,'1920 x 1080','16:9',16.7,9489000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH011','ASUS PRO ART PB248Q','Asus',24,'1920 x 1080','16:9',16.7,7889000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH012','ASUS PRO ART PB287Q','Asus',28,'3840 x 2160','16:9',1070,14689000,36,'Refresh rate: 60Hz,
Response time: 1ms,Panel: TN'),
('MH013','ASUS PRO ART PB328Q','Asus',32,'2560 x 1440','16:9',1070,16989000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: VA'),

('MH014','BENQ GAMING 24 144HZ 1MS XL2430T 144HZ 1MS XL2411Z','BenQ',24,'1920 x 1080','16:9',16.7,9990000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH015','BBENQ DESIGNER 32 4K BL3201PT','BenQ',32,'3840 x 2160','16:9',1070,22979000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: IPS'),

('MH016','BENQ GAMING 27 2K 144HZ 1MS XL2730Z','BenQ',27,'2560 x 1440','16:9',16.7,14979000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH017','BENQ HOME & OFFICE 28 GC2870H','BenQ',28,'1920 x 1080','16:9',16.7,5079000,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: VA'),
('MH018','DELL 22 PROFESSIONAL P2217H LED IPS','Dell',22,'1920 x 1080','16:9',16.7,3300000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH019','DELL 22 S2240T TOUCH WIDE LED','Dell',22,'1920 x 1080','16:9',16.7,6590000,36,'Refresh rate: 60Hz,
Response time: 12ms,Panel: VA'),
('MH020','DELL 24 ULTRASHARP U2412M IPS','Dell',24,'1920 x 1200','16:10',16.7,5590000,36,'Refresh rate: 60Hz,
Response time: 8ms,Panel: IPS'),
('MH021','DELL 24 PROFESSIONAL P2415Q 4K IPS','Dell',24,'3840 x 2160','16:9',1070,10199000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH022','DELL 24 PROFESSIONAL P2417H LED IPS','Dell',24,'1920 x 1080','16:9',16.7,4290000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH023','DELL 25 ULTRASHARP U2515H LED IPS','Dell',25,'2560 x 1440','16:9',16.7,7199000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH024','DELL 27 PROFESSIONAL P2715Q 4K IPS','Dell',27,'3840 x 2160','16:9',1070,14490000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH025','DELL 29 ULTRASHARP U2913WM IPS','Dell',29,'2560 x 1080','21:9',16.7,9590000,36,'Refresh rate: 60Hz,
Response time: 8ms,Panel: IPS'),
('MH026','SAMSUNG 24 C24F390F CURVED LED','Samsung',24,'1920 x 1080','16:9',16.7,4589000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: VA'),
('MH027','DELL 27 ULTRASHARP U2715H LED IPS','Dell',27,'2560 x 1440','16:9',16.7,11649000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH028','DELL 27 ULTRASHARP UP2715K 5K IPS','Dell',27,'5120 x 2880','16:9',1070,41990000,36,'Refresh rate: 60Hz,
Response time: 8ms,Panel: IPS'),
('MH029','SAMSUNG 24 S24F350FH LED PLS','Samsung',24,'1920 x 1080','16:9',16.7,3799000,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: PLS'),
('MH030','SAMSUNG 27 S27D590C CURVED LED','Samsung',27,'1920 x 1080','16:9',16.7,7999000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: VA'),
('MH031','SAMSUNG 27 S27E360H LED PLS','Samsung',27,'1920 x 1080','16:9',16.7,6499000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: PLS'),
('MH032','DELL 28 PROFESSIONAL P2815Q LED','Dell',28,'3840 x 2160','16:9',1070,14590000,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: TN'),
('MH033','DELL 34 ULTRASHARP U3415W IPS CURVED','Dell',34,'3440 x 1440','21:9',1070,22000000,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH034','SAMSUNG 27 S27E390H LED PLS','Samsung',27,'1920 x 1080','16:9',16.7,6499000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: PLS'),
('MH035','SAMSUNG 27 S27E591C CURVED LED','Samsung',27,'1920 x 1080','16:9',16.7,7999000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: VA'),
('MH036','SAMSUNG 27 S27F350FH LED','Samsung',27,'1920 x 1080','16:9',16.7,5899000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: PLS'),
('MH037','SAMSUNG 32 C32F391FW CURVED LED','Samsung',32,'1920 x 1080','16:9',16.7,9999000,36,'Refresh rate: 60Hz,
Response time: 4ms,Panel: VA'),
('MH038','DELL 23 PROFESSIONAL P2317H LED IPS','Dell',23,'1920 x 1080','16:9',16.7,3850000,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: IPS'),
('MH039','Dell E2216H','Dell',22,'1920 x 1080','16:9',16.7,2827984,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: TN'),
('MH040','Dell E2316H','Dell',23,'1920 x 1080','16:9',16.7,3252186,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: TN'),
('MH041','LG 20MP48A-P-AH-IPS','LG',20,'1440 x 900','16:9',16.7,3252186,36,'Refresh rate: 60Hz,
Response time: 14ms,Panel: AH-IPS'),
('MH042','AOC C2783FQ','AOC',27,'1920 x 1080','16:9',16.7,7499998,36,'Refresh rate: 144Hz,
Response time: 5ms,Panel: VA'),
('MH043','AOC G2460PQU','AOC',24,'1920 x 1080','16:9',16.7,5500000,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TFT LCD'),
('MH044','AOC G2770PQU','AOC',27,'1920 x 1080','16:9',16.7,7499998,36,'Refresh rate: 144Hz,
Response time: 1ms,Panel: TN'),
('MH045','AOC I2476V','AOC',24,'1920 x 1080','16:9',16.7,3149982,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH046','AOC P2370SH','AOC',23,'1920 x 1080','16:9',16.7,2999986,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: TN'),
('MH047','AOC I2276V','AOC',22,'1920 x 1080','16:9',16.7,2369994,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH048','BenQ GW2255','BenQ',22,'1920 x 1080','16:9',16.7,2579984,36,'Refresh rate: 60Hz,
Response time: 6ms,Panel: VA'),
('MH049','LG 22MP48','LG',22,'1920 x 1080','16:9',16.7,2749992,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH050','LG 24MP58VQ','LG',24,'1920 x 1080','16:9',16.7,3399993,36,'Refresh rate: 60Hz,
Response time: 2ms,Panel: IPS'),
('MH051','LG 24MP56HQ','LG',24,'1920 x 1080','16:9',16.7,3500006,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH052','BenQ GL2023A','BenQ',20,'1600 x 900','16:9',16.7,1870000,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: TN'),
('MH053','BenQ DL2020','BenQ',20,'1366 x 768','16:9',16.7,1870000,36,'Refresh rate: 76Hz,
Response time: 5ms,Panel: TN'),
('MH054','LG 20MP47A-P','LG',20,'1440 x 900','16:10',16.7,2059994,36,'Refresh rate: 60Hz,
Response time: 5ms,Panel: IPS'),
('MH055','LG 19M37A-B','LG',19,'1366 x 768','16:9',16.7,1919986,36,'Refresh rate: 60Hz,
Response time: 3.5ms,Panel: TN'),
('MH056','AOC E2050','AOC',20,'1600 x 900','16:9',16.7,1889998,36,'Refresh rate: 75Hz,
Response time: 5ms,Panel: TN');


INSERT INTO CPU(ID,Ten,ThuongHieu,Socket,Gia,BaoHanh,GhiChu) values

('CPU001','Intel Core i7-5960X Extreme 3.0GHz','Intel','LGA 2011-v3',24999920,36,'Core Name: Haswell-E,
Cores: 8-Core, Threads:16, L2 Cache:8 x 256KB, L3 Cache:20MB'),
('CPU002','Intel Core i7-5930K 3.5GHz','Intel','LGA 2011-v3',14199988,36,'Core Name: Haswell-E,
Cores: 6-Core, Threads:12, L2 Cache:6 x 256KB, L3 Cache:15MB'),
('CPU003','Intel Core i7-5820K 3.3GHz','Intel','LGA 2011-v3',9399984,36,'Core Name: Haswell-E,
Cores: 6-Core, Threads:12, L2 Cache:6 x 256KB, L3 Cache:15MB'),
('CPU004','Intel Xeon E5-1620V3 3.5GHz','Intel','LGA 2011-v3',7799990,36,'Core Name: Haswell-EP,
Cores: Quad-Core, L2 Cache:4 x 256KB, L3 Cache:10MB'),
('CPU005','AMD Vishera FX 9590 4.7GHz ( 5.0GHz Turbo )','AMD','AM3+',5199986,36,'Core: Vishera,
Multi-Core: Eight-Core,L2 Cache: 8MB'),
('CPU006','Intel Xeon E3-1230V5 3.4GHz (3.8GHz Turbo Boost )','Intel','LGA 1151',6299986,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:8, L3 Cache: 8MB'),
('CPU007','Intel Core i7-6700K 4.0GHz (4.4GHz Turbo Boost )','Intel','LGA 1151',8299984,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:8,L2 Cache: 4 x 256KB, L3 Cache:8MB shared'),
('CPU008','Intel Core i5-6600K 3.5GHz (3.9GHz Turbo Boost )','Intel','LGA 1151',5799992,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:4,L2 Cache: 4 x 256KB, L3 Cache:6MB shared'),
('CPU009','AMD Kaveri A6-7400K Black Edition 3.5GHz ( 3.9Ghz Turbo )','AMD','FM2+',1149984,36,'Core Name: Kaveri,
Cores: Dual-Core, Threads:2,L2 Cache: 1MB'),
('CPU010','Intel Xeon E5-2630 V4 2.1GHz','Intel','LGA 2011',16599990,36,'
Cores: 10-Core,L3 Cache: 25MB'),
('CPU011','Intel Core i7-6700 3.4GHz (4.0GHz Turbo Boost )','Intel','LGA 1151',7429994,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:8, L2 Cache: 4 x 256KB, L3 Cache: 8MB'),
('CPU012','Intel Core i7-6950X 3.0Ghz','Intel','LGA 2011-v3',41499920,36,'Core Name: Broadwell-E,
Cores: 10-Core, Threads:20, L2 Cache: 4 x 256KB, L3 Cache: 25MB'),
('CPU013','Intel Xeon E5-2620 V4 2.1GHz','Intel','LGA 2011',10599996,36,'
Cores: 8-Core, L3 Cache: 20MB'),
('CPU014','Intel Core i5-6600 3.3GHz (3.9GHz Turbo Boost )','Intel','LGA 1151',5399988,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:4, L2 Cache: 4 x 256KB, L3 Cache: 6MB shared'),
('CPU015','Intel Core i7-6900K 3.2Ghz','Intel','LGA 2011-v3',25800060,36,'Core Name: Broadwell-E,
Cores: 8-Core, Threads:16, L3 Cache: 20MB'),
('CPU016','Intel Core i5-6500 3.2GHz (3.6GHz Turbo Boost )','Intel','LGA 1151',4809992,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:4, L2 Cache: 4 x 256KB, L3 Cache: 6MB shared'),
('CPU017','Intel Core i7-6850K 3.6Ghz','Intel','LGA 2011-v3',15099986,36,'Core Name: Broadwell-E,
Cores: 6-Core, Threads:12, L3 Cache: 15MB'),
('CPU018','Intel Core i5-6400 2.7GHz (3.3GHz Turbo Boost )','Intel','LGA 1151',4439996,36,'Core Name: SkyLake,
Cores: Quad-Core, Threads:4, L2 Cache: 4 x 256KB, L3 Cache: 6MB shared'),
('CPU019','Intel Core i7-6800K 3.4Ghz','Intel','LGA 2011-v3',10399994,36,'Core Name: Broadwell-E,
Cores: 6-Core, Threads:12, L3 Cache: 15MB'),
('CPU020','Intel Xeon E3-1231V3 3.4GHz (3.7GHz Turbo Boost )','Intel','LGA 1150',5949988,36,'
Multi-Core: Quad-Core, L3 Cache: 8MB'),
('CPU021','Intel Xeon E3-1220V3 3.1GHz (3.4GHz Turbo Boost )','Intel','LGA 1150',4799982,36,'Core Name: Haswell,
Cores: Quad-Core, L3 Cache: 8MB'),
('CPU022','Intel Core i3-6100 3.7GHz','Intel','LGA 1151',2789996,36,'Core Name: SkyLake,
Cores: Dual-Core, Threads:4, L2 Cache: 2 x 256KB, L3 Cache: 3MB'),
('CPU023','Intel Core i7-4790K 4.0GHz (4.4GHz Turbo Boost )','Intel','LGA 1150',8250000,36,'Core Name: Haswell,
Cores: Quad-Core, Threads:8, L3 Cache: 8MB'),
('CPU024','Intel Pentium G4400 3.3GHz','Intel','LGA 1151',1389982,36,'Core Name: SkyLake,
Cores: Dual-Core, Threads:2, L2 Cache: 2 x 256KB, L3 Cache: 3MB'),
('CPU025','Intel Core i7-4790 3.6GHz (4.0GHz Turbo Boost )','Intel','LGA 1150',7399986,36,'Core Name: Haswell,
Cores: Quad-Core, L2 Cache: 4 x 256KB, L3 Cache: 8MB'),
('CPU026','Intel Core i5-4460 3.1GHz (3.4GHz Turbo Boost )','Intel','LGA 1150',4250004,36,'Core Name: Haswell,
Cores: Quad-Core, L2 Cache: 4 x 256KB, L3 Cache: 6MB'),
('CPU027','Intel Core i3-4160 3.6GHz','Intel','LGA 1150',2479994,36,'Core Name: Haswell,
Cores: Dual-Core, Threads: 4, L2 Cache: 2 x 256KB, L3 Cache: 3MB'),
('CPU028','Intel Xeon E5-2630 V3 2.4GHz','Intel','LGA 2011-v3',16799992,36,'Core Name: Haswell-EP,
Cores: 8-Core, Threads: 16, L2 Cache: 8 x 256KB, L3 Cache: 20MB'),
('CPU029','AMD Athlon II X4-651K BE Unlocked 3.0GHz','FM1','LGA 2011-v3',1899999,36,'
Cores: 4-Core, Threads: 4, L2 Cache: 1MB'),
('CPU030','Intel Pentium G3450 3.4GHz','Intel','LGA 1150',1519980,36,'Core Name: Haswell,
Cores: Dual-Core, Threads:2, L2 Cache: 2 x 256KB, L3 Cache: 3MB'),
('CPU031','Intel Pentium G3260 3.3GHz','Intel','LGA 1150',1199990,36,'Core Name: Haswell,
Cores: Dual-Core, Threads:2, L2 Cache: 2 x 256KB, L3 Cache: 3MB'),
('CPU032','Intel Pentium G3250 3.2GHz','Intel','LGA 1150',1249996,36,'Core Name: Haswell,
Cores: Dual-Core, Threads:2, L2 Cache: 2 x 256KB, L3 Cache: 3MB');

INSERT INTO RAM(ID,Ten,ThuongHieu,Gia,BaoHanh,Loai,Bus,DungLuong,GhiChu) values

('RAM001','Corsair Vengeance Red LED','Cosair',2499992,36,'DDR4',2666,'2x8GB','Speed: DDR4 2666(PC4-21300), Voltage: 1.2V'),
('RAM002','Mushkin Enhanced Proline','Mushkin',1449998,60,'DDR3',1600,'1x8GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.5V'),
('RAM003','Mushkin Enhanced Sodim','Mushkin',550000,60,'DDR3',1600,'1x4GB','Speed: DDR3 1866(PC3-14900), Voltage: 1.5V'),
('RAM004','Mushkin Enhanced Sodim','Mushkin',974996,36,'DDR3',1866,'1x8GB','Speed: DDR3 1866(PC3-14900), Voltage: 1.5V'),
('RAM005','Mushkin Enhanced Sodim','Mushkin',1100000,36,'DDR3',1866,'2x4GB','Speed: DDR3 1866(PC3-14900), Voltage: 1.5V'),
('RAM006','Mushkin Enhanced Sodim','Mushkin',1949992,36,'DDR3',1866,'2x8GB','Speed: DDR3 1866(PC3-14900), Voltage: 1.5V'),
('RAM007','Mushkin Enhanced Sodim Low','Mushkin',519992,36,'DDR3',1600,'1x4GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM008','Gskill Ripjaws V','G.SKILL',2200000,36,'DDR4',2133,'2x8GB','Speed: DDR4 2213(PC4 17000), Voltage: 1.2V'),
('RAM009','Crucial Sodimm','Crucial',679998,36,'DDR3',1600,'1x4GB','Speed: DDR3 1600(PC3L 12800), Voltage: 1.35V'),
('RAM010','Kingston HyperX Fury','HyperX',660000,36,'DDR4',2133,'1x4GB','Speed: DDR4 2133(PC3 17000), Voltage: 1.2V'),
('RAM011','Corsair Vengeance White LED','Corsair',2499992,36,'DDR4',2666,'2x8GB','Speed: DDR4 2666(PC4-21300), Voltage: 1.2V'),
('RAM012','Avexir ROG Impact','Avexir',2400002,60,'DDR4',2666,'2x4GB','Speed: DDR4 2666 (PC4 21300), Voltage: 1.2V'),
('RAM013','Avexir ROG Impact','Avexir',3700004,60,'DDR4',2666,'2x8GB','Speed: DDR4 2666 (PC4 21300), Voltage: 1.2V'),
('RAM014','Avexir ROG Red Tesla','Avexir',5899982,60,'DDR4',2666,'2x8GB','Speed: DDR4 2666 (PC4 21300), Voltage: 1.2V'),
('RAM015','Avexir Green Tesla','Avexir',2799984,60,'DDR4',3000,'2x4GB','Speed: DDR4 3000 (PC4 24000), Voltage: 1.35V'),
('RAM016','Avexir Core Series Blue Led','Avexir',1289992,36,'DDR4',2400,'2x4GB','Speed: DDR4 2400 (PC4 19200), Voltage: 1.2V'),
('RAM017','Avexir Core Series Blue Led','Avexir',2200000,36,'DDR4',2400,'2x8GB','Speed: DDR4 2400 (PC4 19200), Voltage: 1.2V'),
('RAM018','Avexir Core Series Red Led','Avexir',3700004,36,'DDR4',2400,'2x16GB','Speed: DDR4 2400 (PC4 19200), Voltage: 1.2V'),
('RAM019','Kingston HyperX Fury','HyperX',1199990,36,'DDR4',2133,'1x8GB','Speed: DDR4 2133 (PC3 17000), Voltage: 1.2V'),
('RAM020','Gskill Ripjaws V','G.SKILL',1249996,36,'DDR4',2133,'2x4GB','Speed: DDR4 2133 (PC4 19200), Voltage: 1.2V'),
('RAM021','Corsair Vengeance LPX','Corsair',1199990,36,'DDR4',2400,'2x4GB','Speed: DDR4 2400 (PC4 19200), Voltage: 1.2V'),
('RAM022','Corsair Vengeance','Corsair',696886,36,'DDR3',1600,'1x4GB','Speed: DDR3 1600 (PC3 12800), Voltage: 1.5V'),
('RAM023','Mushkin Enhanced Blackline Extreme V2','Mushkin',699996,36,'DDR4',2400,'1x4GB','Speed: 2400 Mhz, Voltage: 1.2V'),
('RAM024','Mushkin Enhanced Blackline Pro','Mushkin',2119999,60,'DDR3',1600,'2x8GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM025','Mushkin Enhanced Blackline Pro','Mushkin',1080000,60,'DDR3',1600,'1x8GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM026','Mushkin Enhanced Blackline Pro','Mushkin',1080000,60,'DDR3',1600,'2x4GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM027','Mushkin Enhanced Blackline','Mushkin',550000,60,'DDR3',1600,'1x4GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM028','Mushkin Enhanced Blackline Pro','Mushkin',574992,60,'DDR4',2400,'1x4GB','Speed: DDR3 2400(PC3-19200), Voltage: 1.2V'),
('RAM029','Mushkin Enhanced Blackline Pro','Mushkin',999988,60,'DDR4',2400,'1x8GB','Speed: DDR3 2400(PC3-19200), Voltage: 1.2V'),
('RAM030','Mushkin Enhanced Essential','Mushkin',1720000,36,'DDR3',1600,'2x8GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM031','Mushkin Enhanced Essential','Mushkin',880000,36,'DDR3',1600,'1x8GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM032','Mushkin Enhanced Essential','Mushkin',880000,36,'DDR3',1600,'2x4GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM033','Mushkin Enhanced Essential','Mushkin',449988,36,'DDR3',1600,'1x4GB','Speed: DDR3 1600(PC3-12800), Voltage: 1.35V'),
('RAM034','Gskill RIPJAWS X Gaming Series','G.SKILL',1199990,36,'DDR3',1866,'2x4GB','Speed: DDR3 1866 (PC3 15000 ), Voltage: 1.5V'),
('RAM035','Gskill RIPJAWS X Gaming Series','G.SKILL',1079980,36,'DDR3',1600,'1x8GB','Speed: DDR3 1600 (PC3 12800 ), Voltage: 1.5V'),
('RAM036','Gskill RIPJAWS X Gaming Series','G.SKILL',639980,36,'DDR3',1600,'1x4GB','Speed: DDR3 1600 (PC3 12800 ), Voltage: 1.5V'),
('RAM037','Gskill TridentZ','G.SKILL',5849998,36,'DDR4',3200,'4x8GB','Speed: DDR4 3200 (PC4 25600), Voltage: 1.35V'),
('RAM038','Gskill TridentZ','G.SKILL',2750000,36,'DDR4',3200,'2x8GB','Speed: DDR4 3200 (PC4 25600), Voltage: 1.35V'),
('RAM039','Gskill Aegis','G.SKILL',649990,36,'DDR4',2133,'1x4GB','Speed: DDR4 2133 (PC4 17000), Voltage: 1.2V'),
('RAM040','Corsair Vengeance LPX Red','Corsair',2589985,36,'DDR4',2400,'2x8GB','Speed: DDR4 2400 (PC4-19200), Voltage: 1.2V'),
('RAM041','Corsair Vengeance LPX','Corsair',2373496,36,'DDR4',2400,'2x8GB','Speed: DDR4 2400 (PC4-19200), Voltage: 1.2V'),
('RAM042','Corsair Vengeance LPX','Corsair',1069992,36,'DDR4',2133,'1x8GB','Speed: DDR4 2133 (PC4 17000), Voltage: 1.2V'),
('RAM043','Corsair Vengeance LPX','Corsair',1333200,36,'DDR4',2133,'2x4GB','Speed: DDR4 2133 (PC4-19200), Voltage: 1.2V'),
('RAM044','Corsair Vengeance LPX','Corsair',2322990,36,'DDR4',2133,'2x4GB','Speed: DDR4 2133 (PC4-19200), Voltage: 1.2V');

INSERT INTO OCung(ID,Ten,ThuongHieu,Gia,BaoHanh,Loai,DungLuong,GhiChu) values
('OC001','Toshiba 1TB 7200rpm','TOSHIBA',1049994,24,'HDD',1024,'Cache: 32MB'),
('OC002','Toshiba 2TB 7200rpm','TOSHIBA',1749990,24,'HDD',2048,'Cache: 64MB'),
('OC003','Western Digital Caviar Black 4TB','Western Digital',5299998,36,'HDD',4096,'Cache: 64MB'),
('OC004','Western Digital Caviar Black 3TB','Western Digital',3999996,36,'HDD',3072,'Cache: 64MB'),
('OC005','Western Digital Caviar Black 2TB','Western Digital',3199988,36,'HDD',2048,'Cache: 64MB'),
('OC006','Western Digital Caviar Black 1TB','Western Digital',1749990,36,'HDD',1024,'Cache: 64MB'),
('OC007','Western Digital Caviar Black 500GB','Western Digital',1489994,36,'HDD',500,'Cache: 64MB'),
('OC008','Western Digital Caviar Red 1TB','Western Digital',1630000,36,'HDD',1024,'Cache: 64MB'),
('OC009','Western Digital Caviar Blue 6TB','Western Digital',5399988,24,'HDD',6144,'Cache: 64MB'),
('OC010','Western Digital Caviar Blue 4TB','Western Digital',3199988,24,'HDD',4096,'Cache: 64MB'),
('OC011','Western Digital Caviar Blue 3TB','Western Digital',2279992,24,'HDD',3072,'Cache: 64MB'),
('OC012','Western Digital Caviar Blue 1TB 7200','Western Digital',1149984,24,'HDD',1024,'Cache: 64MB'),
('OC013','Western Digital Caviar Blue 1TB 5400','Western Digital',1109988,24,'HDD',1024,'Cache: 64MB'),
('OC014','Western Digital Caviar Blue 500GB','Western Digital',1060000,24,'HDD',500,'Cache: 64MB'),
('OC015','Western Digital Caviar Blue 250GB','Western Digital',1089990,24,'HDD',250,'Cache: 64MB'),
('OC016','Western Digital Caviar Green 4TB','Western Digital',3599992,24,'HDD',4096,'Cache: 64MB'),
('OC017','Western Digital Caviar Green 3TB','Western Digital',2510002,24,'HDD',3072,'Cache: 64MB'),
('OC018','Western Digital Caviar Green 2TB','Western Digital',1899986,24,'HDD',2048,'Cache: 64MB'),
('OC019','SSD Mushkin Enhanced Chronos GO','Mushkin',6000000,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 515MB/s'),
('OC020','SSD Crucial M550','Crucial',1600000,36,'SSD',128,'Thoi gian su dung: 1.5tr h, Toc do doc : 550 MB/s, Toc do ghi: 350MB/s'),
('OC021','Panram Velocity','Panram',1290000,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 540 MB/s, Toc do ghi: 390MB/s'),
('OC022','SSD Mushkin Enhanced Reactor 7mm','Mushkin',950000,36,'SSD',1024,'Thoi gian su dung: 2tr h, Toc do doc : 560 MB/s, Toc do ghi: 460MB/s'),
('OC023','SSD Mushkin Enhanced Chronos','Mushkin',3000000,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 515MB/s'),
('OC024','SSD Mushkin Enhanced Striker 7mm','Mushkin',4250004,36,'SSD',480,'Thoi gian su dung: 2tr h, Toc do doc : 565 MB/s, Toc do ghi: 550MB/s'),
('OC025','SSD Mushkin Enhanced Striker 7mm','Mushkin',2299990,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 565 MB/s, Toc do ghi: 335MB/s'),
('OC026','SSD Mushkin Enhanced Triactor','Mushkin',1899986,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 560 MB/s, Toc do ghi: 515MB/s'),
('OC027','SSD Mushkin Enhanced Atlas Deluxe','Mushkin',3200000,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 560 MB/s, Toc do ghi: 530MB/s'),
('OC028','SSD Mushkin Enhanced Chronos G2 7mm','Mushkin',2200000,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 535MB/s'),
('OC029','SSD Mushkin Enhanced Chronos 7mm','Mushkin',2099988,36,'SSD',240,'Thoi gian su dung: 2tr h, Toc do doc : 560 MB/s, Toc do ghi: 525MB/s'),
('OC030','SSD Mushkin Enhanced Chronos G2 7mm','Mushkin',1349986,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 525MB/s'),
('OC031','SSD Samsung 850 EVO','SAMSUNG',4749998,60,'SSD',500,'Thoi gian su dung: 2tr h, Toc do doc : 540 MB/s, Toc do ghi: 520MB/s'),
('OC032','SSD Plextor M7V','Plextor',1929994,36,'SSD',256,'Thoi gian su dung: 1.5tr h, Toc do doc : 535 MB/s, Toc do ghi: 335MB/s'),
('OC033','SSD Mushkin Enhanced Chronos 7mm','Mushkin',1300000,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 515MB/s'),
('OC034','SSD Samsung 850 EVO','SAMSUNG',2649988,60,'SSD',250,'Thoi gian su dung: 2tr h, Toc do doc : 540 MB/s, Toc do ghi: 520MB/s'),
('OC035','SSD Plextor M7V','Plextor',1199990,36,'SSD',128,'Thoi gian su dung: 1.5tr h, Toc do doc : 560 MB/s, Toc do ghi: 500MB/s'),
('OC036','SSD Samsung 850 EVO','SAMSUNG',1650000,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 540 MB/s, Toc do ghi: 520MB/s'),
('OC037','SSD Plextor M6 Pro','Plextor',6549994,60,'SSD',512,'Thoi gian su dung: 2.4tr h, Toc do doc : 545 MB/s, Toc do ghi: 490MB/s'),
('OC038','SSD Plextor M6 Pro','Plextor',3190000,60,'SSD',256,'Thoi gian su dung: 2.4tr h, Toc do doc : 545 MB/s, Toc do ghi: 490MB/s'),
('OC039','SSD Plextor M6S+','Plextor',2499992,36,'SSD',256,'Thoi gian su dung: 1.5tr h, Toc do doc : 520 MB/s, Toc do ghi: 420MB/s'),
('OC040','Avexir SSD S100 Red Led','Avexir',2099988,36,'SSD',120,'Thoi gian su dung: 2tr h, Toc do doc : 550 MB/s, Toc do ghi: 275MB/s'),
('OC041','SSD Plextor M6S+','Plextor',1500000,36,'SSD',128,'Thoi gian su dung: 1.5tr h, Toc do doc : 520 MB/s, Toc do ghi: 300MB/s'),
('OC042','SSD Plextor M6V','Plextor',3799994,36,'SSD',512,'Thoi gian su dung: 2tr h, Toc do doc : 535 MB/s, Toc do ghi: 455MB/s'),
('OC043','Avexir SSD E100','Avexir',1850000,36,'SSD',240,'Thoi gian su dung: 1tr h, Toc do doc : 550 MB/s, Toc do ghi: 520MB/s'),
('OC044','SSD Plextor M6V','Plextor',2194984,36,'SSD',256,'Thoi gian su dung: 1.5tr h, Toc do doc : 535 MB/s, Toc do ghi: 335MB/s'),
('OC045','SSD Plextor M6V','Plextor',1349999,36,'SSD',128,'Thoi gian su dung: 1.5tr h, Toc do doc : 535 MB/s, Toc do ghi: 170MB/s'),
('OC046','SSD OCZ Trion','Trion',1199990,36,'SSD',120,'Thoi gian su dung: 1.5tr h, Toc do doc : 550 MB/s, Toc do ghi: 450MB/s'),
('OC047','SSD OCZ Trion','Trion',2699994,36,'SSD',240,'Thoi gian su dung: 1.5tr h, Toc do doc : 550 MB/s, Toc do ghi: 450MB/s'),
('OC048','SSD Corsair Force LE','Corsair',8599984,36,'SSD',960,'Thoi gian su dung: 1.5tr h, Toc do doc : 560 MB/s, Toc do ghi: 530MB/s'),
('OC049','SSD Corsair Force LE','Corsair',3849993,36,'SSD',480,'Thoi gian su dung: 1.5tr h, Toc do doc : 560 MB/s, Toc do ghi: 530MB/s'),
('OC050','SSD Corsair Force LE','Corsair',2029997,36,'SSD',240,'Thoi gian su dung: 1.5tr h, Toc do doc : 560 MB/s, Toc do ghi: 530MB/s');

INSERT INTO VGA (ID,Ten,ThuongHieu,GPU,BoNho,MinimumPower,Gia,BaoHanh,GhiChu) values
('VGA001', 'Palit Nvidia GT 710 2GB ( 64 Bit ) DDR3', 'Palit' , 'Nvidia GT 710' , '2GB' , 300, 799986 , 36 , 'Memory Interface:64bit. Graphics Clock: 954MHz. Memory Clock: 800MHz (DDR 1600MHz)'),

('VGA002','PowerColor R7 240 1GB ( 128 bit ) DDR5', 'PowerColor' , 'Radeon R7 240' , '1GB' , 400, 1349986  , 36 , 'Engine Clock:830MHz / 880MHz.Memory Clock:1150MHz\(4.6Gbps).Memory Interface:128bit'),

('VGA003', 'Palit Nvidia GT 730 1GB ( 64 Bit ) DDR5', 'Palit' , 'Nvidia GT 730' , '1GB' , 300, 1379994 , 36 , 'Memory Interface:64bit. Graphics Clock:902.Memory Clock:2500MHz (DDR 5000MHz)'),

('VGA004', 'Palit Nvidia GT 730 2GB ( 64 Bit ) DDR5', 'Palit' , 'Nvidia GT 730' , '2GB' , 300, 1379994 , 36 , 'Memory Interface:64bit. Graphics Clock:902.Memory Clock:2500MHz (DDR 5000MHz)'),

('VGA005', 'Gigabyte GT 730 2GB ( 64 bit ) DDR5', 'Gigabyte' , 'Nvidia GT 730' , '2GB' , 300, 1650000  , 36 , 'Memory Interface:64bit. Graphics Clock:902.Memory Clock: 2500MHz (DDR 5000MHz)'),

('VGA006', 'Palit Nvidia GTX 750 Ti StormX 1GB ( 128 Bit ) DDR5', 'Palit' , 'Nvidia GTX 750 Ti' , '1GB' , 300, 2449986 , 36 , 'Memory Interface:128bit.Graphics lock:1085MHz.Memory Clock:2700MHz (DDR 5400MHz)'),

('VGA007', 'Asus R7 260X Direct CU II OC 1GB ( 128 Bit ) DDR5', 'Asus' , 'Radeon R7 260X' , '1GB' , 450, 2600004 , 36 , 'Engine Clock:1075MHz / 880MHz.Memory Clock:1600MHz (6400MHz effective).Memory Interface:128bit'),

('VGA008', 'Palit Nvidia GTX 750 Ti StormX 2GB ( 128 Bit ) DDR5', 'Palit' , 'Nvidia GTX 750 Ti' , '2GB' , 300, 2750000 , 36 , 'Memory Interface:128bit.Graphics lock:1085MHz.Memory Clock:3004MHz (DDR 6008MHz)'),

('VGA009', 'Gigabyte R7 250X OC 2GB ( 128 bit ) DDR5', 'Gigabyte' , 'Radeon R7 250X' , '2GB' , 450, 2750000 , 36 , 'Engine Clock:1020 MHz.Memory Clock:4500 MHz.Memory Interface:128bit'),

('VGA010', 'Asus R7 260X Direct CU II OC 2GB ( 128 Bit ) DDR5', 'Asus' , 'Radeon R7 260X' , '2GB' , 450, 2799984 , 36 , 'Engine Clock:1188MHz.Memory Clock:1750MHz (7000MHz effective).Memory Interface:128bit'),

('VGA011', 'Palit Nvidia GTX 1050 StormX 2GB ( 128 Bit ) DDR5 ', 'Palit' , 'Nvidia GTX 1050' , '2GB' , 300, 2950002 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1455MHz.Memory Clock:3500MHz (DDR 7000MHz)'),

('VGA012', 'Gigabyte GTX 750 Ti WindForce OC 2GB ( 128 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 750 Ti' , '2GB' , 400, 3150004 , 36 , 'Memory Interface:128bit.Graphics lock:1059/ 1137 MHz.Memory Clock:5400 MHz'),

('VGA013', 'MSI Nvidia GTX 750 Ti Twin Frozr 2GB ( 128 Bit ) DDR5', 'MSI' , 'Nvidia GTX 750 Ti' , '2GB' , 400, 3199988 , 36 , 'Memory Interface:128bit.Graphics lock:1059/ 1137 MHz.Memory Clock:5400 MHz'),

('VGA014', 'Asus Nvidia GTX 750 Ti Strix Edition 2GB ( 128 Bit ) DDR5', 'Asus' , 'Nvidia GTX 750 Ti' , '2GB' , 400, 3300000, 36 , 'Memory Interface:128bit.Graphics lock:1059/ 1137 MHz.Memory Clock:5400 MHz'),

('VGA015', 'Palit Nvidia GTX 950 StormX Dual 2GB ( 128 Bit ) DDR5', 'Palit' , 'Nvidia GTX 950' , '2GB' , 350, 3449996  , 36 , 'Memory Interface:128bit.Graphics clock:1064MHz / Boost Clock : 1241MHz.Memory Clock:3305MHz (DDR 6610MHz)'),

('VGA016', 'Palit Nvidia GTX 1050 Ti StormX 4GB ( 128 Bit ) DDR5 ', 'Palit' , 'Nvidia GTX 1050 Ti' , '4GB' , 350, 3500002 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1455MHz.Memory Clock:3500MHz (DDR 7000MHz)'),

('VGA017', 'Sapphire RX 460 Nitro+ OC 4GB ( 128 Bit ) DDR5', 'Sapphire' , 'Radeon RX 460' , '4GB' , 450, 3579994 , 36 , 'Engine Clock:1250 MHz/1175 MHz.Memory Clock:1750 MHz .Memory Interface:128bit'),

('VGA018', 'Gigabyte Geforce GTX 1050 OC 2GB ( 128bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1050' , '2GB' , 300, 3609980 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1455MHz.Memory Clock:3500MHz (DDR 7000MHz)'),

('VGA019', 'Asus Nvidia GTX 950 Dual Fan OC 2GB ( 128 Bit ) DDR5', 'Asus' , 'Nvidia GTX 950' , '2GB' , 400, 4099986  , 36 , 'Memory Interface:128bit.Graphics clock:1064MHz / Boost Clock : 1241MHz.Memory Clock:6610 MHz'),

('VGA020', 'Gigabyte Geforce GTX 1050 TI 4GB ( 128bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1050 Ti' , '4GB' , 350, 4149992 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1455MHz.Memory Clock:3500MHz (DDR 7000MHz)'),

('VGA021', 'Palit Nvidia GTX 960 JetStream 2GB ( 128 Bit ) DDR5', 'Palit' , 'Nvidia GTX 960' , '2GB' , 400,  4299988 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1266MHz.Memory Clock:3600MHz (DDR 7000MHz)'),

('VGA022', 'Palit Nvidia GTX 960 JetStream 4GB ( 128 Bit ) DDR5', 'Palit' , 'Nvidia GTX 960' , '4GB' , 400,  4800004 , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1266MHz.Memory Clock:3600MHz (DDR 7000MHz)'),

('VGA023', 'Gigabyte Geforce GTX 1050 TI G1 Gaming 4GB ( 128bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1050 Ti' , '4GB' , 350,  4950000  , 36 , 'Memory Interface:128bit.Graphics clock:1354MHz / Boost Clock : 1455MHz.Memory Clock:3500MHz (DDR 7000MHz)'),

('VGA024', 'Nvidia Quadro Maxwell K620 - Workstation Video Card', 'Nvidia' , 'Quadro K620' , '2GB' , 350,  5149980  , 36 , 'CUDA Cores: 384. DDR3'),

('VGA025', 'HIS RX 470 IceQ X� OC 4GB ( 256 bit ) DDR5', 'HIS' , 'Radeon RX 470' , '4GB' , 500, 5299998 , 36 , 'Engine Clock:1250 MHz.Memory Clock:7000 MHz .Memory Interface:256bit'),

('VGA026', 'Asus Nvidia GTX 960 Strix Edition 2GB ( 128 Bit ) DDR5', 'Asus' , 'Nvidia GTX 960' , '4GB' , 400,  5500000 , 36 , 'Memory Interface:128bit.Graphics clock:1228MHz / Boost Clock : 1266MHz.Memory Clock: 7200MHz'),

('VGA027', 'Palit Nvidia GTX 1060 Dual Fan 3GB ( 192 Bit ) DDR5', 'Palit' , 'Nvidia GTX 1060' , '3GB' , 400,  5500000 , 36 , 'Memory Interface:192bit.1506MHz / Boost Clock : 1708MHz.Memory Clock: 4000MHz (effective 8000MHz)'),

('VGA028', 'MSI Nvidia GTX 1060 3GT OC ( 192 Bit ) DDR5', 'MSI' , 'Nvidia GTX 1060' , '3GB' , 400, 5710540 , 36 , 'Memory Interface:192bit.1506MHz / Boost Clock : 1708MHz.Memory Clock: 4000MHz (effective 8000MHz)'),

('VGA029', 'Gigabyte Geforce GTX 1060 D5 3GB ( 192 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1060' , '3GB' , 400, 5899982 , 36 , 'Memory Interface:192bit.1506MHz / Boost Clock : 1708MHz.Memory Clock: 4000MHz (effective 8000MHz)'),

('VGA030', 'Asus RX480 Dual Fan OC 4GB ( 256 Bit ) DDR5', 'Asus' , 'Radeon RX 480' , '4GB' , 500, 5900004 , 36 , 'Engine Clock:1300 MHz.Memory Clock:7000 MHz .Memory Interface:256bit'),

('VGA031', 'Sapphire RX 470 Nitro+ RGB Led OC 8GB ( 256 Bit ) DDR5', 'Sapphire' , 'Radeon RX 470' , '8GB' , 500, 6099984 , 36 , 'Engine Clock:1250 MHz/1175 MHz.Memory Clock:2000 MHz,8000Mbps .Memory Interface:256bit'),

('VGA032', 'MSI Nvidia GTX 1060 Tiger OC 6GB ( 192 Bit ) DDR5', 'MSI' , 'Nvidia GTX 1060' , '6GB' , 400, 6899992 , 36 , 'Memory Interface:192bit.1506MHz / Boost Clock : 1708MHz.Memory Clock: 4000MHz (effective 8000MHz)'),

('VGA033', 'Sapphire RX 480 Nitro+ RGB Led OC 8GB ( 256 Bit ) DDR5', 'Sapphire' , 'Radeon RX 480' , '8GB' , 500, 7299996 , 36 , 'Engine Clock:1208 MHz/1342 MHz  MHz.Memory Clock:2000 MHz,8000Mbps .Memory Interface:256bit'),

('VGA034', 'Gigabyte RX 480 8GB ( 256 bit ) DDR5', 'Gigabyte' , 'Radeon RX 480' , '8GB' , 500, 7349980 , 36 , 'Engine Clock:1266 MHz/1120 MHz.Memory Clock:2000 MHz,8000Mbps .Memory Interface:256bit'),

('VGA035', 'Gigabyte Geforce GTX 1060 G1 Gaming 6GB ( 192 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1060' , '6GB' , 400, 8100004 , 36 , 'Memory Interface:192bit.Boost: 1809 MHz/ Base: 1594 MHz.Memory Clock: 8008MHz'),

('VGA036', 'Asus Nvidia GTX 1060 Strix OC RGB LED 6GB ( 192 Bit ) DDR5', 'Asus' , 'Nvidia GTX 1060' , '6GB' , 400, 8989992 , 36 , 'Memory Interface:192bit.Boost: 1847MHz/ Base: 1620 MHz.Memory Clock: 8208 MHz'),

('VGA037', 'HIS R9 390X IceQ X2 8GB ( 512 bit ) DDR5', 'HIS' , 'Radeon R9 390X' , '8GB' , 750, 10499984 , 36 , 'Memory Interface:512bit'),

('VGA038', 'Palit Nvidia GTX 1070 Dual Fan 8GB ( 256 Bit ) DDR5', 'Palit' , 'Nvidia GTX 1070' , '8GB' , 500,  10499984 , 36 , 'Memory Interface:256bit.1506MHz / Boost Clock : 1683MHz.Memory Clock: 4000MHz (effective 8000MHz)'),

('VGA039', 'Nvidia Quadro K1200 - Workstation Video Card', 'Nvidia' , 'Quadro K1200' , '4GB' , 350,   11199980  , 36 , 'CUDA Cores : 512. GDDR5'),

('VGA040', 'Gigabyte Geforce GTX 1070 G1 Gaming 8GB ( 256 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1070' , '8GB' , 500, 11990000 , 36 , 'Memory Interface:256bit.Boost: 1809 MHz/ Base: 1594 MHz.Memory Clock: 8008MHz'),

('VGA041', 'Nvidia Quadro M2000 - Workstation Video Card', 'Nvidia' , 'Quadro M2000' , '4GB' , 350,  11990000  , 36 , 'CUDA Cores : 768. GDDR5'),

('VGA042', 'Asus Nvidia GTX 1070 Strix 8GB Gaming ( 256 Bit ) DDR5', 'Asus' , 'Nvidia GTX 1070' , '8GB' , 500, 12449998 , 36 , 'Memory Interface:256bit.Boost: 1809 MHz/ Base: 1594 MHz.Memory Clock: 8008MHz'),

('VGA043', 'Nvidia Quadro Maxwell K2200 - Workstation Video Card', 'Nvidia' , 'Quadro K2200' , '4GB' , 350,  13799984  , 36 , 'CUDA Cores : 640. GDDR5'),

('VGA044', 'Gigabyte GTX 980 G1 Gaming 4GB ( 256 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 980' , '4GB' , 550, 15149992 , 36 , 'Memory Interface:256bit.Boost: 1329 MHz/ Base: 1228 MHz.Memory Clock: 7010MHz'),

('VGA045', 'Palit Nvidia GTX 1080 Super JetStream RGB Led 8GB ( 256 Bit ) DDR5', 'Palit' , 'Nvidia GTX 1080' , '8GB' , 500,  17199996  , 36 , 'Memory Interface:256bit.Base Clock : 1708MHz / Boost Clock : 1847MHz.Memory Clock: 5000MHz (effective 10000MHz)'),

('VGA046', 'Nvidia Quadro Fermi 4000', 'Nvidia' , 'Quadro 4000' , '2GB' , 400,  17499988  , 36 , 'CUDA Cores : 256. GDDR5'),

('VGA047', 'Asus Nvidia GTX 1080 Strix A8GB Gaming ( 256 Bit ) DDR5', 'Asus' , 'Nvidia GTX 1080' , '8GB' , 500,  17799980  , 36 , 'Memory Interface:256bit.Base Clock : 1708MHz / Boost Clock : 1847MHz.Memory Clock: 5000MHz (effective 10000MHz)'),

('VGA048', 'Gigabyte GTX 980Ti Extreme 6GB ( 384 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 980Ti' , '6GB' , 600, 17899992 , 36 , 'Memory Interface:384bit.Boost: 1329 MHz/ Base: 1228 MHz.Memory Clock: 7210MHz'),

('VGA049', 'Gigabyte Geforce GTX 1080 Xtreme Gaming Premium 8GB ( 256 bit ) DDR5', 'Gigabyte' , 'Nvidia GTX 1080' , '8GB' , 500,  19849984  , 36 , 'Memory Interface:256bit.Boost: 1898 MHz / Base: 1759 MHz in Gaming mode.Memory Clock: 10211 MHz'),

('VGA050', 'Asus Nvidia GTX 980Ti Gold Anniversary Limited Edition 6GB ( 384 Bit ) DDR5', 'Asus' , 'Nvidia GTX 1080' , '6GB' , 600,  22000000  , 36 , 'Memory Interface:384bit.Boost: 1367 MHz.Base Clock : 1266 MHz.Memory Clock: 7200 MHz'),

('VGA051', 'Nvidia Quadro M4000 - Workstation Video Card', 'Nvidia' , 'Quadro M4000' , '8GB' , 450,  24999920  , 36 , 'CUDA Cores : 1664. GDDR5'),

('VGA052', 'Gigabyte TitanX Xtreme 12GB ( 384 bit ) DDR5', 'Gigabyte' , 'GTX TITAN X' , '12GB' , 600,  27500000  , 36 , 'Memory Interface:384bit.Boost:1266 MHz/ Base: 1165 MHz in Gaming Mode'),

('VGA053', 'Nvidia Quadro M5000 - Workstation Video Card', 'Nvidia' , 'Quadro M5000' , '8GB' , 500,  49500000  , 36 , 'CUDA Cores : 2048. GDDR5'),

('VGA054', 'Nvidia Quadro Tesla K40 - Workstation Video Card', 'Nvidia' , 'Quadro Tesla K40' , '12GB' , 500,  99900020  , 36 , 'CUDA Cores : 2880. GDDR5'),

('VGA055', 'Nvidia Quadro M6000 - Workstation Video Card', 'Nvidia' , 'Quadro M6000' , '12GB' , 500,  129000080  , 36 , 'CUDA Cores : 3072. GDDR5');


INSERT INTO MainBoard values 

('MAIN001', 'Asrock H81M-DGS - Haswell LGA 1150' , 'Asrock' , 'LGA 1150' , 'DDR3' , '1600/1333/1066' , 2  , 1149984 ,36, 'Asrock H81M-DGS - Haswell LGA 1150'),
('MAIN002', 'Asus H81M-E - LGA 1150' , 'Asus' , 'LGA 1150' , 'DDR3' , '1600/1333/1066' , 2  , 1349986 ,36, 'Asus H81M-E - LGA 1150'),
('MAIN003', 'Gigabyte H81M-DS2 - LGA 1150' , 'Gigabyte' , 'LGA 1150' , 'DDR3' , '1600/1333' , 2  ,  1379994  ,36, 'Gigabyte H81M-DS2 - LGA 1150'),
('MAIN004', 'Asrock B85M-Pro4 - Haswell LGA 1150' , 'Asrock' , 'LGA 1150' , 'DDR3' , '1600/1333/1066' , 4  , 1449998 ,36, 'Asrock B85M-Pro4 - Haswell LGA 1150'),
('MAIN005', 'Asrock H110M-DVS R2.0 - Skylake LGA 1151' , 'Asrock' , 'LGA 1151' , 'DDR4' , '2133' , 4  ,  1549988 ,36, 'Asrock H110M-DVS R2.0 - Skylake LGA 1151'),
('MAIN006', 'Asus H110M-E - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '2133' , 2  ,  1629980 ,36, 'Asus H110M-E - LGA 1151 Skylake'),
('MAIN007', 'Gigabyte H110M-S2PV-DDR3 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR3' , '1600/1333' , 2  ,  1630000 ,36, 'Supports 6th Generation Intel� Core� Processor. Dual Channel DDR3/ DDR3L, 2 DIMMs. 8-channel HD Audio with High Quality Audio Capacitors'),
('MAIN008', 'Asus B85M-Gamer - LGA 1150' , 'Asus' , 'LGA 1150' , 'DDR3' , '1600/1333/1066' , 4  , 1799996  ,36, 'Asus B85M-Gamer - LGA 1150'),
('MAIN009', 'MSI H110 Pro VD - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 1850000  ,36, 'MSI H110 Pro VD - LGA 1151 Skylake'),
('MAIN010', 'Gigabyte B150M-HD3-DDR3 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR3/DDR3L' , '1866(OC)/1600/1333' , 2  , 1919984 ,36, 'Supports 6th Generation Intel� Core� Processor. Dual Channel DDR3/DDR3L, 2 DIMMs'),
('MAIN011', 'Asrock B150M Pro4/D3 - Skylake LGA 1151' , 'Asrock' , 'LGA 1151' , 'DDR3/DDR3L' , '1866(OC)/1600/1333/1066' , 4  , 1949992 ,36, 'Asrock B150M Pro4/D3 - Skylake LGA 1151'),
('MAIN012', 'Asrock Z97M Anniversary - Haswell LGA 1150' , 'Asrock' , 'LGA 1150' , 'DDR3' , '3100+(OC)/2933(OC)/2800(OC)/2400(OC)/2133(OC)/1866(OC)/1600/1333/1066' , 4  , 1999998   ,36, 'Asrock Z97M Anniversary - Haswell LGA 1150'),
('MAIN013', 'MSI B150M Gaming Pro - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 2189990 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket. Supports DDR4-2133 Memory'),
('MAIN014', 'MSI E3M WORKSTATION V5 - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 2249984 ,36, 'Supports Intel� Xeon� E3 v5 series / Core� i3 / Pentium� / Celeron� processors for LGA 1151 socket. Supports ECC DDR4 Memory'),
('MAIN015', 'MSI B150M Bazoka - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 2319988 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket. Supports DDR4-2133 Memory'),
('MAIN016', 'Asus B85 Pro Gamer - LGA 1150' , 'Asus' , 'LGA 1150' , 'DDR3' , '1600/1333/1066 ' , 4  , 2389992 ,36, 'Intel� for 4th Generation Core� i7/Core� i5/Core� i3/Pentium�/Celeron� Processors'),
('MAIN017', 'Asus B150M Pro Gaming - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '2133' , 4  ,  2389992 ,36, 'Intel� for 6th Generation Core� i7/Core� i5/Core� i3/Pentium�/Celeron� Processors '),
('MAIN018', 'MSI B150M Mortar - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 2399980 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN019', 'Asrock B150M Pro4/Hyper - Skylake LGA 1151' , 'Asrock' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 2449986 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN020', 'Gigabyte B150M-HD3-DDR3 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 2599982 ,36, 'Supports the Intel� Xeon� E3-1200 v5 processor and 6th Gen. Intel� Core� i3/ Pentium�/ Celeron� Processor. Dual Channel DDR4, 2 DIMMs'),
('MAIN021', 'Asrock Fatal1ty H97 Killer - Haswell LGA 1150' , 'Asrock' , 'LGA 1150' , 'DDR3' , '1600/1333/1066' , 4  , 2649988,36, ' Supports 5th Generation Intel� Core� i7/i5/i3/Pentium�/Celeron� Processors (Socket 1150)'),
('MAIN022', 'ASRock Fatal1ty B150 Gaming K4 - LGA 1151 Skylake' , 'Asrock' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 2689984 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN023', 'Gigabyte B150 G1 Sniper B7 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 2750000 ,36, 'Supports the Intel� Xeon� E3-1200 v5 processor and 6th Gen. Intel� Core� i3/ Pentium�/ Celeron� Processor. Dual Channel DDR4, 2 DIMMs'),
('MAIN024', 'ASRock Fatal1ty H170 Performance - LGA 1151 Skylake' , 'Asrock' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 2999986 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN025', 'ASRock Fatal1ty H170 Performance - LGA 1151 Skylake' , 'Asrock' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 3069990 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN026', 'Gigabyte X150 Plus WS- LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '2133' , 2  , 3099998 ,36, 'Supports the Intel� Xeon� E3-1200 v5 processor and 6th Gen. Intel� Core� i3/ Pentium�/ Celeron� Processor. Dual Channel DDR4, 4 DIMMs'),
('MAIN027', 'MSI B150A Gaming Pro - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 3149982 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN028', 'Gigabyte H170-Gaming 3 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 3339996 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN029', 'MSI H170A Gaming Pro RGB Led - LGA 1151 Skylake' , 'MSI' , 'LGA 1151' , 'DDR4' , '2133' , 4  , 3449996 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN030', 'Asus B150 Pro Gaming Aura - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '2133' , 4  ,  3499980 ,36, 'Intel� for 6th Generation Core� i7/Core� i5/Core� i3/Pentium�/Celeron� Processors '),
('MAIN031', 'Gigabyte Z97-D3H - LGA 1150' , 'Gigabyte' , 'LGA 1150' , 'DDR3' , '3100(OC)/1333' , 4  , 3643552 ,36, 'Core i7 / i5 / i3 / Pentium / Celeron (LGA1150)'),
('MAIN032', 'Gigabyte Z170-D3H - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '3466/3400/3333/3300/3200/3000/2800/2666/2400/2133' , 4  , 3099998 ,36, 'Support for Intel� Core� i7 processors/Intel� Core� i5 processors/Intel� Core� i3 processors/Intel�Pentium� processors/Intel� Celeron� processors in the LGA1151 package'),
('MAIN033', 'Asus E3 Pro Gaming V5 - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '2133' , 4  ,   3799994 ,36, 'LGA1151 socket for Intel� Xeon� E3-1200 v5 processors and 6th-genereation Core, Pentium� and Celeron� processors'),
('MAIN034', 'Gigabyte Z170X-Gaming 3 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '3300/ 3200/ 3000/ 2800/ 2666/ 2400/ 2133' , 4  , 4329996 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN035', 'Asus Z170 Pro Gaming - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '3400/3333/3200/3100/3000/2933/2800/2666/2600/2400/2133' , 4  ,   3799994 ,36, 'Core i7 / i5 / i3 / Pentium / Celeron (LGA1151)'),
('MAIN036', ' Gigabyte Z170X-Gaming 5 - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '3466/3400/3333/3300/3200/3000/2800/2666/2400/2133' , 4  , 4950000 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN037', ' Gigabyte X99-UD4 - LGA 2011-V3 Intel X99' , 'Gigabyte' , 'LGA2011-v3' , 'DDR4' , '3000(O.C.)/2800(O.C.)/2666(O.C.)/2400(O.C.)/2133' , 8  , 5799992 ,36, 'Supports New Intel� Core� i7 Processor Extreme Edition'),
('MAIN038', 'Asrock X99-Taichi 2011 v3' , 'Asrock' , 'LGA2011-v3' , 'DDR4' , '3300/2933/2800/2400/2133' , 8  , 6099984 ,36, 'Xeon / Core i7 (LGA2011-v3)'),
('MAIN039', 'Asus X99-A - LGA 2011-V3 Intel X99' , 'Asus' , 'LGA2011-v3' , 'DDR4' , '3200(O.C.)/3000(O.C.)/2800(O.C.)/2666(O.C.)/2400(O.C.)/2133' , 8  ,  6299986 ,36, 'Core i7 (LGA2011-v3)'),
('MAIN040', ' Gigabyte Z170X-Gaming GT - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' ,'3866/3733/3666/36003466/3400/3333/3300/3200/3000/2800/2666/2400/2133' , 4  , 6849986 ,36, 'Supports 6th Gen Intel� Core� / Pentium� / Celeron� processors for LGA 1151 socket'),
('MAIN041', 'Gigabyte X99-Ultra Gaming - LGA 2011-V3 Intel X99' , 'Gigabyte' , 'LGA2011-v3' , 'DDR4' , ' 3600/3400/3333/3200/3000/2800/2666/2400/2133' , 8  , 8299984 ,36, 'Supports New Intel� Core� i7 Processor Extreme Edition'),
('MAIN042', 'Gigabyte Z170X-SOC Force - LGA 1151 Skylake' , 'Gigabyte' , 'LGA 1151' , 'DDR4' , '3866/3733/3666/3600/3466/3400/3333/3300/3200/3000/2800/2666/2400/2133' , 4  ,  10300004 ,36, 'Core i7 / i5 / i3 / Pentium / Celeron (LGA1151)'),
('MAIN043', 'Gigabyte GA-X99-Designare EX - LGA 2011-V3 Intel X99' , 'Gigabyte' , 'LGA2011-v3' , 'DDR4' , ' 3600/3400/3333/3200/3000/2800/2666/2400/2133' , 8  , 10499984 ,36, 'Supports New Intel� Core� i7 Processor Extreme Edition'),
('MAIN044', 'Asus Z170 Maximus VIII Extreme/Assembly - LGA 1151 Skylake' , 'Asus' , 'LGA 1151' , 'DDR4' , '3866/3733/3666/3600/3466/3400/3333/3300/3200/3000/2800/2666/ 2400/2133' , 4  ,   18499998 ,36, 'Core i7 / i5 / i3 / Pentium / Celeron (LGA1151)');


INSERT INTO PSU values 
('PSU001', 'Acbel HK350 - PSU' , 'Acbel' , 350, 379984 , 24 , '20+4pins , 3connectort , 3SATA ,v er2.0ATX , fan 8cm , 4pins . C�ng su?t th?c hon 200W '),
('PSU002', 'Antec BP300S - True Power', 'Antec',300 , 474980 , 24, 'ATX12V ver2.3 ;100~240V. 80%.1 x 24(20+4)-pin. .1 x 4 pin ATX12V.1 x 6 pin PCI-E. 3 x SATA.2 x Molex. 1 x Floppy'),
('PSU003', 'Antec BP350S - True Power','Antec', 350 , 550000 , 24 ,'Type: ATX12V v2.0.Main Connector. 20+4Pin'),
('PSU004', 'In-Win PowerMan IP-S 400W Peak 480W - True Power PSU' , 'In-win', 400 , 550000 , 24 ,'Type: ATX12V v2.0.Main Connector. 20+4Pin'),
('PSU005', ' FSP Saga 450 400W - Active PFC' , 'FSP' , 400 , 699996 , 24 ,'400w'),
('PSU006', ' FSP Saga 500 - 450W - Active PFC' , 'FSP' , 450 ,  779988  , 24 ,'450w'),
('PSU007', 'Andyson M5 500W Japan Cap Single Rail - 80 Plus Bronze PSU' , 'Andyson' , 500 , 899998 ,36 ,'12V+  444W / 500W '),
('PSU008', 'Corsair Builder VS550 550W - 85% Efficiency - Active PFC' , 'Corsair' , 550 , 999989 , 36, ' 85% , Active PFC . ( Single Rail )  .'),
('PSU009', 'Corsair CXV3 430W - Single Rail - 80 Plus Bronze PSU', 'Corsair' , 430 , 1069982  , 36 , '80 Plus Bronze Certified.'),
('PSU010', ' Andyson M5 700W Japan Cap - Single Rail - 80 Plus Bronze PSU', 'Andyson' , 700,  1199990 , 36 , '700w'),
('PSU011', 'Antec Neo ECO II 550W' , 'Antec' , 550, 1349986 , 36, '80 PLUS� BRONZE .PCI-E 8(6+2pin) x 2/ CPU 4+4pin/ SATA x 6'),
('PSU012', 'Seasonic S12II 520 - 80 Plus Bronze PSU' , 'Seasonic' , 520 , 1500000 , 36 ,'20+4Pin. 2 x 6+2-pin. 82-85%. 80 Plus Bronze Certificated'),
('PSU013', 'Corsair CXV3 600W - Single Rail - 80 Plus Bronze PSU' , 'Corsair' , 600, 1599996 , 36 , '80 PLUS BRONZE Certified'),
('PSU014', 'Corsair CXV3 750W - Single Rail - 80 Plus Bronze PSU' , 'Corsair' , 750, 2098768 , 36 , '80 PLUS BRONZE Certified'),
('PSU015', 'Antec HCG - 850M 850W 80Plus Bronze' , 'Antec' ,  850 , 2699994 , 36 , '100~240V (Active PFC + 10%). '),
('PSU016', 'Corsair RM Series 650W - 80 Plus Gold - Full Modular' , 'Corsair' , 650, 3089980  , 36 , '92 %, 80 PLUS GOLD Certified'),
('PSU017', 'Corsair RM Series 850X - 80 Plus Gold - Full Modular' , 'Corsair' , 850, 3990001  , 36 , '90 %, 80 PLUS GOLD Certified'),
('PSU018', 'In-Win Classic 750W Aluminium - Full Modular 80Plus Platinium Premium PSU' , 'In-win', 400 , 3999996 , 60 ,'92%. '),
('PSU019', 'Corsair RM Series 1000X - 80 Plus Gold - Full Modular' , 'Corsair' , 1000, 4689998 , 36 , '90 %, 80 PLUS GOLD Certified'),
('PSU020', 'Andyson R1200 1200W - Full Modular 80 Plus Platinium PSU' , 'Andyson' , 1200, 4999984 , 60 ,'Full Modular 80 Plus Platinium PSU' ),
('PSU021', 'Antec HCP 1300P 1300W', 'Antec' , 1300, 6899992 , 36, '80 Plus Platinum . 94% '),
('PSU022', 'Corsair AX1200i 1200W Profesional Platinium Series' , 'Corsair' , 1200, 7869991 , 60 , '92 %, 80 PLUS PLATIUM Certified'),
('PSU023', 'Corsair AX1500i 1500W Profesional Platinium Series' , 'Corsair' , 1500, 10959993 , 60 , '94 %, 80 PLUS TITANIUM Certified');
