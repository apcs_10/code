[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(PCBuilder3.App_Start.Combres), "PreStart")]
namespace PCBuilder3.App_Start {
	using System.Web.Routing;
	using global::Combres;
	
    public static class Combres {
        public static void PreStart() {
            RouteTable.Routes.AddCombresRoute("Combres");
        }
    }
}