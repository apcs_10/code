﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PCBuilder3.Models;

namespace PCBuilder3.Controllers
{
    public class StoreController : Controller
    {
        public Cart UCart { get; set; }
        // GET: Store
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                string user = System.Web.HttpContext.Current.User.Identity.GetUserId();
                DataClasses1DataContext db = new DataClasses1DataContext();
                var fcart = (from search in db.Carts
                             where search.UID == user
                             select search);
                if (fcart.Count() == 0)
                {
                    Cart ncart = new Cart()
                    {
                        UID = user
                    };
                    db.Carts.InsertOnSubmit(ncart);
                    db.SubmitChanges();
                    UCart = ncart;
                }
                else
                {
                    foreach (Cart res in fcart)
                    {
                        UCart = res;
                    }
                }
            }
            return View();
        }

        public ActionResult Category(int cat)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            switch (cat)
            {
                case 1:
                    var res = (from search in db.MainBoards
                               orderby search.Gia descending
                               select search);
                    List<string> lres = new List<string>();
                    foreach (MainBoard rres in res)
                    {
                        lres.Add(rres.ID.ToString());
                        lres.Add(rres.Ten.ToString());
                        lres.Add(rres.Gia.ToString());
                    }
                    lres.Add("MainBoard");
                    ViewBag.res = Json(lres,JsonRequestBehavior.AllowGet);
                    return View();
                    //return Json(lres, JsonRequestBehavior.AllowGet);
                    break;
                case 2:
                    var cpures = (from search in db.CPUs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres2 = new List<string>();
                    foreach (CPU rres in cpures)
                    {
                        lres2.Add(rres.ID.ToString());
                        lres2.Add(rres.Ten.ToString());
                        lres2.Add(rres.Gia.ToString());
                    }
                    lres2.Add("CPU");
                    ViewBag.res = Json(lres2, JsonRequestBehavior.AllowGet);
                    return View();
                case 3:
                    var ramres = (from search in db.RAMs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres3 = new List<string>();
                    foreach (RAM rres in ramres)
                    {
                        lres3.Add(rres.ID.ToString());
                        lres3.Add(rres.Ten.ToString());
                        lres3.Add(rres.Gia.ToString());
                    }
                    lres3.Add("RAM");
                    ViewBag.res = Json(lres3, JsonRequestBehavior.AllowGet);
                    return View();

                case 4:
                    var mhres = (from search in db.ManHinhs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres4 = new List<string>();
                    foreach (ManHinh rres in mhres)
                    {
                        lres4.Add(rres.ID.ToString());
                        lres4.Add(rres.Ten.ToString());
                        lres4.Add(rres.Gia.ToString());
                    }
                    lres4.Add("Monitor");
                    ViewBag.res = Json(lres4, JsonRequestBehavior.AllowGet);
                    return View();
                case 5:
                    var vgares = (from search in db.VGAs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres5 = new List<string>();
                    foreach (VGA rres in vgares)
                    {
                        lres5.Add(rres.ID.ToString());
                        lres5.Add(rres.Ten.ToString());
                        lres5.Add(rres.Gia.ToString());
                    }
                    lres5.Add("VGA");
                    ViewBag.res = Json(lres5, JsonRequestBehavior.AllowGet);
                    return View();
                case 6:
                    var hdres = (from search in db.OCungs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres6 = new List<string>();
                    foreach (OCung rres in hdres)
                    {
                        lres6.Add(rres.ID.ToString());
                        lres6.Add(rres.Ten.ToString());
                        lres6.Add(rres.Gia.ToString());
                    }
                    lres6.Add("HardDrive");
                    ViewBag.res = Json(lres6, JsonRequestBehavior.AllowGet);
                    return View();
                case 7:
                    var psures = (from search in db.PSUs
                                  orderby search.Gia descending
                                  select search);
                    List<string> lres7 = new List<string>();
                    foreach (PSU rres in psures)
                    {
                        lres7.Add(rres.ID.ToString());
                        lres7.Add(rres.Ten.ToString());
                        lres7.Add(rres.Gia.ToString());
                    }
                    lres7.Add("PSU");
                    ViewBag.res = Json(lres7, JsonRequestBehavior.AllowGet);
                    return View();
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int cat, string ID)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            switch (cat)
            {
                case 1:
                    var res = (from search in db.MainBoards
                               orderby search.ID == ID
                               select search);
                    MainBoard resmain = new MainBoard();
                    foreach (MainBoard rres in res)
                    {
                        resmain = rres;
                    }
                    ViewBag.res = Json(resmain, JsonRequestBehavior.AllowGet);
                    return View();
                    break;
                case 2:
                    var cpures = (from search in db.CPUs
                                  orderby search.ID == ID
                                  select search);
                    CPU rescpu = new CPU();
                    foreach (CPU rres in cpures)
                    {
                        rescpu = rres;
                    }
                    ViewBag.res = Json(rescpu, JsonRequestBehavior.AllowGet);
                    return View();
                case 3:
                    var ramres = (from search in db.RAMs
                                  orderby search.ID == ID
                                  select search);
                    RAM resram = new RAM();
                    foreach (RAM rres in ramres)
                    {
                        resram = rres;
                    }
                    ViewBag.res = Json(resram, JsonRequestBehavior.AllowGet);
                    return View();

                case 4:
                    var mhres = (from search in db.ManHinhs
                                 orderby search.ID == ID
                                 select search);
                    ManHinh resmh = new ManHinh();
                    foreach (ManHinh rres in mhres)
                    {
                        resmh = rres;
                    }
                    ViewBag.res = Json(resmh, JsonRequestBehavior.AllowGet);
                    return View();
                case 5:
                    var vgares = (from search in db.VGAs
                                  orderby search.ID == ID
                                  select search);
                    VGA resvga = new VGA();
                    foreach (VGA rres in vgares)
                    {
                        resvga = rres;
                    }
                    ViewBag.res = Json(resvga, JsonRequestBehavior.AllowGet);
                    return View();
                case 6:
                    var hdres = (from search in db.OCungs
                                 orderby search.ID == ID
                                 select search);
                    OCung reshd = new OCung();
                    foreach (OCung rres in hdres)
                    {
                        reshd = rres;
                    }
                    ViewBag.res = Json(reshd, JsonRequestBehavior.AllowGet);
                    return View();
                case 7:
                    var psures = (from search in db.PSUs
                                  orderby search.ID == ID
                                  select search);
                    PSU respsu = new PSU();
                    foreach (PSU rres in psures)
                    {
                        respsu = rres;
                    }
                    ViewBag.res = Json(respsu, JsonRequestBehavior.AllowGet);
                    return View();
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}