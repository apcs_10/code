﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PCBuilder3.Models;

namespace PCBuilder3.Controllers
{
    public class BuildController : Controller
    {
        // GET: Build
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Build(double budget, int type)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            double cpu = 0;
            double main = 0;
            double ram;
            double hdd = 0;
            double ssd = 0;
            double psu = 0;
            double vga = 0;
            List<string> lres = new List<string>();
            //Office
            if (type == 1)
            {
                //CPU
                cpu = (double)(30 * budget / 100);

                var cpures = (from search in db.CPUs
                              where (search.Gia <= cpu && (search.Loai == 1 || search.Loai == 2)) 
                              orderby search.Gia descending
                              select search).Take(1);
                List<CPU> cpusearchres = new List<CPU>();
                foreach (CPU res in cpures)
                {
                    cpusearchres.Add(res);
                }

                //MAIN
                if (budget <= 5000000) main = 1600000;
                else if (budget <= 8000000) main = 2000000;
                else if (budget <= 10000000) main = 2500000;
                else if (budget <= 15000000) main = 2800000;
                else if (budget <= 20000000) main = 3300000;
                else main = 4000000;
                var mainres = (from search in db.MainBoards
                               where (search.Gia <= main && search.Loai >= cpusearchres[0].Loai && search.Socket == cpusearchres[0].Socket)
                               orderby search.Gia descending
                               select search).Take(1);
                List<MainBoard> mainsearchres = new List<MainBoard>();
                foreach (MainBoard res in mainres)
                {
                    mainsearchres.Add(res);
                }

                //HDD
                if (budget <= 7000000) hdd = 1100000;
                else if (budget <= 10000000) hdd = 1200000;
                else if (budget <= 20000000) hdd = 2000000;
                else hdd = 4000000;
                var hddres = (from search in db.OCungs
                              where (search.Gia <= hdd && search.Loai == "HDD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> hddsearchres = new List<OCung>();
                foreach (OCung res in hddres)
                {
                    hddsearchres.Add(res);
                }

                //SSD
                if (budget <= 7000000) ssd = 0;
                else if (budget <= 15000000) ssd = 128;
                else if (budget <= 30000000) ssd = 256;
                else if (budget <= 40000000) ssd = 512;
                else ssd = 1024;
                var ssdprice = 0;
                if (budget <= 7000000) ssdprice = 0;
                else if (budget <= 15000000) ssdprice = 1300000;
                else if (budget <= 20000000) ssdprice = 2000000;
                else if (budget <= 30000000) ssdprice = 3000000;
                else if (budget <= 35000000) ssdprice = 5000000;
                else if (budget <= 40000000) ssdprice = 8000000;
                else ssdprice = 10000000;
                var ssdres = (from search in db.OCungs
                              where (search.DungLuong <= ssd && search.Gia <= ssdprice && search.Loai == "SSD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> ssdsearchres = new List<OCung>();
                foreach (OCung res in ssdres)
                {
                    ssdsearchres.Add(res);
                }

                //PSU
                if (budget <= 7000000) psu = 500000;
                else if (budget <= 10000000) psu = 700000;
                else if (budget <= 15000000) psu = 900000;
                else if (budget <= 20000000) psu = 1000000;
                else if (budget <= 30000000) psu = 2000000;
                else psu = 10000000;
                var psures = (from search in db.PSUs
                              where (search.Gia <= psu)
                              orderby search.Gia descending
                              select search).Take(1);
                List<PSU> psusearchres = new List<PSU>();
                foreach (PSU res in psures)
                {
                    psusearchres.Add(res);
                }


                //    if (budget <= 10000000) ram = "4GB";
                //    else if (budget <= 15000000) ram = "8GB";
                //    else ram = "16GB";
                lres.Add(mainsearchres[0].ID.ToString());
                lres.Add(mainsearchres[0].Ten.ToString());
                lres.Add(mainsearchres[0].Gia.ToString());
                lres.Add(cpusearchres[0].ID.ToString());
                lres.Add(cpusearchres[0].Ten.ToString());
                lres.Add(cpusearchres[0].Gia.ToString());
                lres.Add(hddsearchres[0].ID.ToString());
                lres.Add(hddsearchres[0].Ten.ToString());
                lres.Add(hddsearchres[0].Gia.ToString());
                lres.Add(psusearchres[0].ID.ToString());
                lres.Add(psusearchres[0].Ten.ToString());
                lres.Add(psusearchres[0].Gia.ToString());
                if (ssdsearchres.Count != 0)
                {
                    lres.Add(ssdsearchres[0].ID.ToString());
                    lres.Add(ssdsearchres[0].Ten.ToString());
                    lres.Add(ssdsearchres[0].Gia.ToString());
                }
                else
                {
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                }
                return Json(lres, JsonRequestBehavior.AllowGet);

            }
            //Gaming
            if (type == 2)
            {
                //CPU
                cpu = (double)(27 * budget / 100);
                var cpures = (from search in db.CPUs
                              where (search.Gia <= cpu && search.Loai != 4 && search.ThuongHieu != "AMD")
                              orderby search.Loai descending, search.Gia descending
                              select search).Take(1);
                List<CPU> cpusearchres = new List<CPU>();
                foreach (CPU res in cpures)
                {
                    cpusearchres.Add(res);
                }

                //VGA
                if (budget <= 7000000) vga = 1700000;
                else if (budget <= 8000000) vga = 2500000;
                else if (budget <= 10000000) vga = 3000000;
                else if (budget <= 15000000) vga = 4000000;
                else if (budget <= 20000000) vga = 7000000;
                else if (budget <= 25000000) vga = 12000000;
                else if (budget <= 30000000) vga = 17000000;
                else if (budget <= 40000000) vga = 18000000;
                else if (budget <= 50000000) vga = 20000000;
                else if (budget <= 60000000) vga = 25000000;
                else vga = 1000000000;
                var vgares = (from search in db.VGAs
                              where (search.Gia <= vga && search.ThuongHieu != "Nvidia")
                              orderby search.Gia descending
                              select search).Take(1);
                List<VGA> vgasearchres = new List<VGA>();
                foreach (VGA res in vgares)
                {
                    vgasearchres.Add(res);
                }

                //MAIN
                if (budget <= 5000000) main = 1600000;
                else if (budget <= 8000000) main = 2000000;
                else if (budget <= 10000000) main = 2500000;
                else if (budget <= 15000000) main = 2800000;
                else if (budget <= 20000000) main = 3300000;
                else main = 4000000;
                var mainres = (from search in db.MainBoards
                               where (search.Gia <= main && search.Loai == cpusearchres[0].Loai)
                               orderby search.Gia descending
                               select search).Take(1);
                List<MainBoard> mainsearchres = new List<MainBoard>();
                foreach (MainBoard res in mainres)
                {
                    mainsearchres.Add(res);
                }

                //HDD
                if (budget <= 7000000) hdd = 1100000;
                else if (budget <= 10000000) hdd = 1200000;
                else if (budget <= 20000000) hdd = 2000000;
                else hdd = 3000000;
                var hddres = (from search in db.OCungs
                              where (search.Gia <= hdd && search.Loai == "HDD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> hddsearchres = new List<OCung>();
                foreach (OCung res in hddres)
                {
                    hddsearchres.Add(res);
                }

                //SSD
                if (budget <= 7000000) ssd = 0;
                else if (budget <= 15000000) ssd = 128;
                else if (budget <= 30000000) ssd = 256;
                else if (budget <= 40000000) ssd = 512;
                else ssd = 1024;
                var ssdprice = 0;
                if (budget <= 7000000) ssdprice = 0;
                else if (budget <= 15000000) ssdprice = 1300000;
                else if (budget <= 20000000) ssdprice = 1800000;
                else if (budget <= 30000000) ssdprice = 2000000;
                else if (budget <= 35000000) ssdprice = 2500000;
                else if (budget <= 40000000) ssdprice = 3000000;
                else ssdprice = 4000000;
                var ssdres = (from search in db.OCungs
                              where (search.DungLuong <= ssd && search.Gia <= ssdprice && search.Loai == "SSD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> ssdsearchres = new List<OCung>();
                foreach (OCung res in ssdres)
                {
                    ssdsearchres.Add(res);
                }

                //PSU
                if (budget <= 7000000) psu = 500000;
                else if (budget <= 10000000) psu = 700000;
                else if (budget <= 15000000) psu = 1000000;
                else if (budget <= 20000000) psu = 1500000;
                else if (budget <= 30000000) psu = 2000000;
                else psu = 1000000000;
                var psures = (from search in db.PSUs
                              where (search.Gia <= psu)
                              orderby search.Gia descending
                              select search).Take(1);
                List<PSU> psusearchres = new List<PSU>();
                foreach (PSU res in psures)
                {
                    psusearchres.Add(res);
                }

                lres.Add(mainsearchres[0].ID.ToString());
                lres.Add(mainsearchres[0].Ten.ToString());
                lres.Add(mainsearchres[0].Gia.ToString());
                lres.Add(cpusearchres[0].ID.ToString());
                lres.Add(cpusearchres[0].Ten.ToString());
                lres.Add(cpusearchres[0].Gia.ToString());
                lres.Add(hddsearchres[0].ID.ToString());
                lres.Add(hddsearchres[0].Ten.ToString());
                lres.Add(hddsearchres[0].Gia.ToString());
                lres.Add(psusearchres[0].ID.ToString());
                lres.Add(psusearchres[0].Ten.ToString());
                lres.Add(psusearchres[0].Gia.ToString());
                if (ssdsearchres.Count != 0)
                {
                    lres.Add(ssdsearchres[0].ID.ToString());
                    lres.Add(ssdsearchres[0].Ten.ToString());
                    lres.Add(ssdsearchres[0].Gia.ToString());
                }
                else
                {
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                }
                return Json(lres, JsonRequestBehavior.AllowGet);

            }

            if (type == 3)
            {
                //CPU
                if (budget <= 10000000) cpu = budget * 43 / 100;
                else if (budget <= 20000000) cpu = budget * 35 / 100;
                else cpu = budget * 30 / 100;
                var cpures = (from search in db.CPUs
                              where (search.Gia <= cpu && search.ThuongHieu != "AMD")
                              orderby search.Loai descending, search.Gia descending
                              select search).Take(1);
                List<CPU> cpusearchres = new List<CPU>();
                foreach (CPU res in cpures)
                {
                    cpusearchres.Add(res);
                }

                //MAIN
                if (budget <= 5000000) main = 1600000;
                else if (budget <= 8000000) main = 2000000;
                else if (budget <= 10000000) main = 2500000;
                else if (budget <= 15000000) main = 3000000;
                else if (budget <= 20000000) main = 4000000;
                else if (budget <= 25000000) main = 4500000;
                else if (budget <= 30000000) main = 7000000;
                else main = 11000000;
                var mainres = (from search in db.MainBoards
                               where (search.Gia <= main && search.Loai == cpusearchres[0].Loai)
                               orderby search.Gia descending
                               select search).Take(1);
                List<MainBoard> mainsearchres = new List<MainBoard>();
                foreach (MainBoard res in mainres)
                {
                    mainsearchres.Add(res);
                }

                //RAM

                //HDD
                if (budget <= 7000000) hdd = 1100000;
                else if (budget <= 10000000) hdd = 1200000;
                else if (budget <= 20000000) hdd = 2000000;
                else hdd = 3000000;
                var hddres = (from search in db.OCungs
                              where (search.Gia <= hdd && search.Loai == "HDD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> hddsearchres = new List<OCung>();
                foreach (OCung res in hddres)
                {
                    hddsearchres.Add(res);
                }

                //SSD
                if (budget <= 7000000) ssd = 0;
                else if (budget <= 15000000) ssd = 128;
                else if (budget <= 30000000) ssd = 256;
                else if (budget <= 40000000) ssd = 512;
                else ssd = 1024;
                var ssdprice = 0;
                if (budget <= 7000000) ssdprice = 0;
                else if (budget <= 15000000) ssdprice = 1300000;
                else if (budget <= 20000000) ssdprice = 1800000;
                else if (budget <= 30000000) ssdprice = 2000000;
                else if (budget <= 35000000) ssdprice = 2500000;
                else if (budget <= 40000000) ssdprice = 3000000;
                else ssdprice = 4000000;
                var ssdres = (from search in db.OCungs
                              where (search.DungLuong <= ssd && search.Gia <= ssdprice && search.Loai == "SSD")
                              orderby search.Gia descending
                              select search).Take(1);
                List<OCung> ssdsearchres = new List<OCung>();
                foreach (OCung res in ssdres)
                {
                    ssdsearchres.Add(res);
                }

                //PSU
                if (budget <= 7000000) psu = 500000;
                else if (budget <= 10000000) psu = 700000;
                else if (budget <= 15000000) psu = 1000000;
                else if (budget <= 20000000) psu = 1500000;
                else if (budget <= 30000000) psu = 2000000;
                else psu = 1000000000;
                var psures = (from search in db.PSUs
                              where (search.Gia <= psu)
                              orderby search.Gia descending
                              select search).Take(1);
                List<PSU> psusearchres = new List<PSU>();
                foreach (PSU res in psures)
                {
                    psusearchres.Add(res);
                }

                //RAM
                //ram = budget - (cpusearchres[0].Gia.Value + mainsearchres[0].Gia.Value + ssdsearchres[0].Gia.Value + hddsearchres[0].Gia.Value + psusearchres[0].Gia.Value);
                //var ramres = (from search in db.RAMs
                //              where (search.Gia <= ram)
                //              orderby search.Gia descending
                //              select search).Take(1);

                lres.Add(mainsearchres[0].ID.ToString());
                lres.Add(mainsearchres[0].Ten.ToString());
                lres.Add(mainsearchres[0].Gia.ToString());
                lres.Add(cpusearchres[0].ID.ToString());
                lres.Add(cpusearchres[0].Ten.ToString());
                lres.Add(cpusearchres[0].Gia.ToString());
                lres.Add(hddsearchres[0].ID.ToString());
                lres.Add(hddsearchres[0].Ten.ToString());
                lres.Add(hddsearchres[0].Gia.ToString());
                lres.Add(psusearchres[0].ID.ToString());
                lres.Add(psusearchres[0].Ten.ToString());
                lres.Add(psusearchres[0].Gia.ToString());
                if (ssdsearchres.Count != 0)
                {
                    lres.Add(ssdsearchres[0].ID.ToString());
                    lres.Add(ssdsearchres[0].Ten.ToString());
                    lres.Add(ssdsearchres[0].Gia.ToString());
                }
                else
                {
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                    lres.Add("No SSD");
                }
                return Json(lres, JsonRequestBehavior.AllowGet);

            }
            return Json(lres, JsonRequestBehavior.AllowGet);
        }
    }


}