﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PCBuilder3.Startup))]
namespace PCBuilder3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
