﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PCBuilder.Startup))]
namespace PCBuilder
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
